import React from "react";
import { signIn, signOut } from "../actions";
import { connect } from 'react-redux'
import { auth, provider } from "./Firebase";
import { signInWithPopup, onAuthStateChanged, signOut as signOutFirebase } from "firebase/auth"

class GoogleAuth extends React.Component {

    componentDidMount(){
        this.onAuthchange(auth.currentUser)
        onAuthStateChanged(auth, this.onAuthchange)
    }

    onSignInClick = () => {
        signInWithPopup(auth, provider)
    }

    onSignOutClick = () => {
        signOutFirebase(auth)
    }

    onAuthchange = isSignedIn => {
        if (isSignedIn) {
            this.props.signIn(auth.currentUser.uid)
        }else{
            this.props.signOut();
        }
    }
    
    renderLoginButton() {
        if (this.props.isSignedIn === null){
            return null
        } else if (this.props.isSignedIn === true) {
            return (
                <button onClick={this.onSignOutClick} className="ui red google button">
                    <i className="google icon" />
                    Sign Out
                </button>
            )
        } else {
            return (
                <button onClick={this.onSignInClick} className="ui red google button">
                    <i className="google icon" />
                    Sign In
                </button>
            )
        }
    }

    render(){
        return <div>{this.renderLoginButton()}</div>
    }
}

const mapStateToProps = state => {
    return {
        isSignedIn: state.auth.isSignedIn
    }
}

export default connect(
    mapStateToProps,
    { signIn, signOut }
)(GoogleAuth);