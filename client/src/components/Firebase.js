// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider } from "firebase/auth"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyACGeDXO1goA1fF9PU-FtYf6JrUp_-73Xg",
  authDomain: "streams-thiago-coelho.firebaseapp.com",
  projectId: "streams-thiago-coelho",
  storageBucket: "streams-thiago-coelho.appspot.com",
  messagingSenderId: "1047960800327",
  appId: "1:1047960800327:web:89a4a3b8306eb35a5b3562"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app)

export const provider = new GoogleAuthProvider(app);
